import React, { createContext, useState, useEffect } from "react";
import uuid from "uuid";

export const PokemonContext = createContext();

const PokemonContextProvider = (props) => {
  const initialState = JSON.parse(localStorage.getItem("pokemons")) || [];

  const [pokemons, setPokemons] = useState(initialState);

  useEffect(() => {
    localStorage.setItem("pokemons", JSON.stringify(pokemons));
  }, [pokemons]);

  //add pokemon to list
  const addPokemon = (pokeName, nickname) => {
    setPokemons([
      ...pokemons,
      { id: uuid(), name: pokeName, nickname: nickname },
    ]);
  };

  //remove pokemon from list
  const removePokemon = (id) => {
    setPokemons(pokemons.filter(poke => poke.id !== id))
  };

  //clear all pokemon
  const clearPokemon = () => {
    setPokemons([])
  }

  return (
    <PokemonContext.Provider
      value= {{
        pokemons,
        addPokemon,
        removePokemon,
        clearPokemon
      }}
    >
      {props.children}
    </PokemonContext.Provider>
  )
};

export default PokemonContextProvider
