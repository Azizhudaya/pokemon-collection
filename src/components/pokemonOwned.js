import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { PokemonContext } from "../contexts/pokemonContext";

function PokemonOwned({ pokemon }) {
  const { removePokemon } = useContext(PokemonContext);
  return (
    <div className="border rounded p-3 m-3 grid md:grid-cols-3 grid-cols-1 gap-4">
      <div className="capitalize my-auto mx-3">{pokemon.nickname}</div>
      <div className="font-bold my-auto uppercase mx-3">
        <Link to={`/pokemon-detail/${pokemon.name}`}>{pokemon.name}</Link>
      </div>
      <div className="flex justify-end">
        <button
          className="border border-red-500 bg-red-500 text-white active:bg-red-600 font-bold text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mx-3 ease-linear transition-all duration-150"
          onClick={() => removePokemon(pokemon.id)}
        >
          Release
        </button>
      </div>
    </div>
  );
}
export default PokemonOwned;
