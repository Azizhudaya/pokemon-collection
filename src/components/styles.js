//taken from https://www.geeksforgeeks.org/how-to-create-a-scroll-to-top-button-in-react-js/

import styled from "styled-components";

export const Button = styled.div`
  position: fixed;
  width: 100%;
  left: 50%;
  bottom: 60px;
  height: 20px;
  font-size: 3rem;
  z-index: 1;
  cursor: pointer;
  color: black;
`;
