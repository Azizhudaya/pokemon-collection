import React, { useContext, useState, useEffect } from "react";
import { PokemonContext } from "../contexts/pokemonContext";

function CatchModal({name}) {
  /*
  showModalState definition
  0: modal close
  1: modal show confirmation to catch pokemon
  2: modal show failed to catch pokemon
  3: modal show success to catch pokemon
  4: modal show name exists
  5: modal show success store to storage
  */
  const { pokemons, addPokemon } = useContext(PokemonContext);
  const [showModalState, setshowModalState] = useState(0);
  const [pokeName, setPokeName] = useState("");

  const catchProbability = () => {
    return Math.random() < 0.5 ? 2 : 3;
  };

  useEffect(() => {
    return () => {
      setshowModalState(0);
      setPokeName("");
    };
  }, []);

  const handleChange = (e) => {
    setPokeName(e.target.value);
  };

  const handleSubmit = () => {
    if (pokemons.find((pokemon) => pokemon.nickname === pokeName) || pokeName === "") {
      setshowModalState(4);
    } else {
      addPokemon(name, pokeName);
      setshowModalState(5);
    }
  };

  return (
    <>
      <button
        className="bg-red-500 text-white active:bg-red-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
        type="button"
        onClick={() => setshowModalState(1)}
      >
        Catch pokemon
      </button>
      {showModalState !== 0 ? (
        <>
          <div className="mx-3 justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 className="text-2xl font-semibold">Confirm</h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setshowModalState(0)}
                  >
                    <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                  <p className="my-4 text-blueGray-500 text-lg leading-relaxed">
                    {showModalState === 1
                      ? "Are you sure to catch this pokemon?"
                      : showModalState === 2
                      ? "Failed to catch pokemon, wanna try again?"
                      : showModalState === 4
                      ? "Found pokemon with same name or without name. Try another name"
                      : showModalState === 3
                      ? "Congrats, now name your pokemon"
                      : "Pokemon stored"}
                  </p>
                  {showModalState === 3 || showModalState === 4 ? (
                    <input
                      type="text"
                      placeholder="pokemon name..."
                      value={pokeName}
                      onChange={handleChange}
                      required
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    />
                  ) : null}
                </div>
                {/*footer*/}
                {showModalState !== 5 ? (
                  <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b gap-4">
                    <button
                      className="border border-red-500 text-red-500 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg mr-1 mb-1 ease-linear transition-all duration-150"
                      type="button"
                      onClick={() => setshowModalState(0)}
                    >
                      {showModalState === 1 || showModalState === 2
                        ? "No"
                        : "Cancel"}
                    </button>
                    <button
                      className="border border-red-500 bg-red-500 text-white active:bg-red-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                      type="button"
                      onClick={() =>
                        showModalState === 1 || showModalState === 2
                          ? setshowModalState(catchProbability)
                          : handleSubmit()
                      }
                    >
                      {showModalState === 1 || showModalState === 2
                        ? "Yes"
                        : "Confirm"}
                    </button>
                  </div>
                ) : (
                  <button
                    className="border border-red-500 bg-red-500 text-white active:bg-red-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none m-3 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setshowModalState(0)}
                  >
                    Close
                  </button>
                )}
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}

export default CatchModal;
