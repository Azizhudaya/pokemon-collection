import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/home";
import PokemonDetail from "./pages/pokemonDetail";
import PokemonList from "./pages/pokemonList";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import PokemonContextProvider from "./contexts/pokemonContext";

function App() {
  const client = new ApolloClient({
    uri: "https://graphql-pokeapi.vercel.app/api/graphql",
  });

  return (
    <Router>
      <div className="max-w-4xl min-h-screen mx-auto p-5">
        <Switch>
          <PokemonContextProvider>
            <Route exact path="/" component={Home} />
            <ApolloProvider client={client}>
              <Route exact path="/pokemon-list" component={PokemonList} />
              <Route exact path="/pokemon-detail/:name" component={PokemonDetail} />
            </ApolloProvider>
          </PokemonContextProvider>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
