import React, { useContext } from "react";
import pokeball from "../images/pokeball.png";
import { Link } from "react-router-dom";
import { PokemonContext } from "../contexts/pokemonContext";
import PokemonOwned from "../components/pokemonOwned";

export default function Home() {
  const { pokemons } = useContext(PokemonContext);
  return (
    //title section
    <div className="border rounded-xl p-5">
      <div className="font-bold text-3xl text-center mb-10">
        Pokemon Collection
      </div>
      <img
        src={pokeball}
        className="mx-auto"
        style={{ width: "150px" }}
        alt="pokeball images"
      />

      <Link to="/pokemon-list">
        <div className="border border-gray-300 p-2 rounded-xl text-center mx-auto max-w-xl mt-10">
          Go to Pokedex
        </div>
      </Link>

      <div>
        <div className="font-bold mt-10 mb-2">Owned Pokemon :</div>
        <div>
          {pokemons.length > 0 ? (
            pokemons.map((pokemon) => {
              return <PokemonOwned pokemon={pokemon} key={pokemon.id} />;
            })
          ) : (
            <div className="">No pokemon owned</div>
          )}
        </div>
      </div>
    </div>
  );
}
