import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { useQuery } from "@apollo/client";
import ScrollButton from '../components/scrollButton';
import { GET_DETAIL_POKEMON } from "../graphql/queryPokemon";
import CatchModal from "../components/catchModal";

function PokemonDetail() {
  const { name } = useParams();
  const { loading, error, data } = useQuery(GET_DETAIL_POKEMON, {
    variables: { name: name },
  });

  let history = useHistory();

  if (loading) return "Loading...";
  if (error) return `Error! ${error.message}`;

  const poke = data.pokemon;
  return (
    <>
      <button onClick={() => history.goBack()}> Back </button>
      <div className="text-3xl font-bold my-3">Detail Pokemon</div>
      <div className="border rounded md:p-5 p-2 mb-5">
        <div>#{("0000" + poke.id).slice(-4)}</div>
        <div className="capitalize text-2xl my-2 font-bold text-center">
          {poke.name}
        </div>
        <div className="flex justify-center space-x-5">
          {poke.types.map(({ type }) => (
            <div key={type.name} className="capitalize mb-2">
              {type.name}
            </div>
          ))}
        </div>
        <div className="flex justify-center space-x-5">
          <div>
            <img
              className=""
              src={poke.sprites.front_default}
              alt={`${poke.name} front images`}
            />
            <div className="text-center">Front image</div>
          </div>
          <div>
            <img
              className=""
              src={poke.sprites.back_default}
              alt={`${poke.name} back images`}
            />
            <div className="text-center">Back image</div>
          </div>
        </div>
        <div className="flex justify-center my-5">
        <CatchModal name={poke.name} />
        </div>
      </div>
      <div>
        <div className="border rounded md:p-5 p-2">
          <div className="mb-5">
            <div className="font-bold">Abilities :</div>
            {poke.abilities.map(({ ability }) => (
              <div key={ability.name} className="ml-3">
                {ability.name}
              </div>
            ))}
          </div>
          <div className="mb-5">
            <div className="font-bold">Moves :</div>
            <div className="grid md:grid-cols-3 grid-cols-2">
              {poke.moves.length > 0 ? poke.moves.map(({ move }) => (
                <div key={move.name} className="ml-3">
                  {move.name}
                </div>
              )) : <div className="ml-3">No move to be listed</div>}
            </div>
          </div>
        </div>
      </div>
      <ScrollButton />
    </>
  );
}

export default PokemonDetail;
