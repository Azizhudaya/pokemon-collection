import React, { useContext } from "react";
import { useQuery } from "@apollo/client";
import { GET_LIST_POKEMONS } from "../graphql/queryPokemon";
import ScrollButton from "../components/scrollButton";
import { useHistory, Link } from "react-router-dom";
import { PokemonContext } from "../contexts/pokemonContext";

function PokemonList() {
  const { loading, error, data } = useQuery(GET_LIST_POKEMONS, {
    variables: { limit: 1118, offset: 0 },
  });

  const { pokemons } = useContext(PokemonContext);
  let history = useHistory();

  const checkPokemonOwned = (name) => {
    const ownedLength = pokemons.filter((pokemon) => pokemon.name === name).length
    if (ownedLength > 0) {
      return (
      <div>
        {ownedLength} {ownedLength > 1 ? "pokemons" : "pokemon"}
      </div>
      )
    } else {
      return null;
    }
  };


  if (loading) return "Loading...";
  if (error) return `Error! ${error.message}`;

  return (
    <>
      <button onClick={() => history.goBack()}> Back </button>
      <div className="text-3xl font-bold my-3">Pokedex</div>
      <div className="grid md:grid-cols-3 grid-cols-2 gap-4">
        {data.pokemons.results.map((poke, index) => (
          <div key={poke.url} className="border-2 p-3  rounded-xl">
            <Link to={`/pokemon-detail/${poke.name}`}>
              <div>{("0000" + (index + 1)).slice(-4)}</div>
              <div className="capitalize text-xl mb-2">{poke.name}</div>
              <img
                className="mx-auto"
                src={poke.image}
                alt={`${poke.name} images`}
              />
              {checkPokemonOwned(poke.name)}
            </Link>
          </div>
        ))}
      </div>
      <ScrollButton />
    </>
  );
}

export default PokemonList;
