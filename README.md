# Pokemon Collection with React and Graphql

## Important Link
Netlify Link : [https://aziz-pokemon-collection.netlify.app](https://aziz-pokemon-collection.netlify.app)\
Gitlab Link : [https://gitlab.com/Azizhudaya/pokemon-collection](https://gitlab.com/Azizhudaya/pokemon-collection)

## Available `yarn` command
### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.